import { Component } from '@angular/core';
import { GithubService } from '../../services/github.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent {
  username: '';
  repo: '';
  issues: '';
  p: number;
  constructor(private github: GithubService) {}

  search() {
    this.github
      .getReposForIssues(this.username, this.repo)
      .subscribe((data) => {
        console.log(data);
        this.issues = data;
      });
  }
}
