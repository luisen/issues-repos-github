# Intalación del proyecto

- El proyecto ha sido generado con Angular Cli y está en la versión Angular 10.0.0.
- `npm install` para cargar todas las dependencias y se pueda levantar el servidor.
- `ng serve -o` para lenvantar el servidor.

# Descripción y uso de la APP

- La APP cuenta con 2 secciones. Que son Inicio y Buscar.
- Inicio es estática y se hace una descripción de la App.
- La sección Buscar hay que introducir en uno de los campos el propietario del repositorio y en el otro, el repositorio de su propiedad que escojas y te muestra todos los issues del repositorio seleccionado en diferentes cards dispuestas en maxonry.
- Se ha utilizado Bootstrap como framework css y la app es totalmente responsive y cuenta con animaciones css.
